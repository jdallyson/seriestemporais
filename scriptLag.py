import random
from Orange.data import Domain, Table, ContinuousVariable
from pandas import DataFrame
from pandas import concat
from Orange.data.pandas_compat import table_from_frame, table_to_frame

df= table_to_frame(in_data) #Carregando os dados importados do CSV
del df['Date'] # removendo a coluna da data
df = concat([df.shift(3), df.shift(2), df.shift(1), df], axis=1)
df = df.drop([0, 1, 2]) # removendo linhas com NAN

#print(df.head(5)) #imprimindo os 5 primeiros
t_2 = ContinuousVariable("t-2")
t_1 = ContinuousVariable("t-1")
t = ContinuousVariable("t")
t_m_1 = ContinuousVariable("t+1")

#definindo os nomes e tipos das novas colunas
domain = in_data.domain
new_domain = Domain(attributes=(t_2, t_1,t,t_m_1), metas=domain.metas,class_vars=domain.class_vars)

#retornando o data_frame com o nome dominio de dados definido
out_data = Table(new_domain, df)